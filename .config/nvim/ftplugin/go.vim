let b:ale_go_gopls_executable = glob('~/go/bin/gopls')
let b:ale_go_gopls_options = ''

let b:ale_go_golangci_lint_options = ''
let b:ale_go_golangci_lint_package = 1

let b:ale_fixers = ['goimports']
let b:ale_linters = ['gopls', 'golangci-lint']
