#############
#  ALIASES  #
#############

# misc
alias :q='exit'
alias update='yay -Syu --devel'

# vim
alias vim='nvim'

# open ranger, cd to current dir on close
ra() {
	tempfile="$(mktemp -t tmp.XXXXXX)"
	ranger --choosedir="$tempfile" "${@:-$(pwd)}"
	test -f "$tempfile" && if [ "$(cat -- "$tempfile")" != "$(echo -n `pwd`)" ]; then
	cd -- "$(cat "$tempfile")"
	fi
	rm -f -- "$tempfile"
}
