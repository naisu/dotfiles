####################
#  AUTOSUGGESTION  #
####################

# https://github.com/zsh-users/zsh-autosuggestions

ZSH_AUTOSUGGEST_USE_ASYNC=1
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20

bindkey '^ ' autosuggest-accept
bindkey '^\' autosuggest-toggle
