###########
#  LOCAL  #
###########

[[ -f ~/.local.profile ]] && \
    source ~/.local.profile


###################
#  ENV VARIABLES  #
###################

# user scripts
export PATH="$HOME/.bin:$PATH"

# go
export GOPATH="$HOME/go"
export GOBIN="$GOPATH/bin"
export PATH="$GOBIN:$PATH"

# yarn
export PATH="$(yarn global bin):$PATH"

# terminal emulator
export TERMINAL='alacritty'

# editor
export VISUAL='nvim'
export EDITOR='nvim'

# lang
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# aurutils
export AUR_REPO='aur'
export AUR_PAGER='ranger'

# nvidia OpenGL cache dir
export __GL_SHADER_DISK_CACHE_PATH="$HOME/.cache/nvidia"

# qt5 theming
export QT_QPA_PLATFORMTHEME='qt5ct'


#################
#  X AUTOSTART  #
#################
if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx
fi
